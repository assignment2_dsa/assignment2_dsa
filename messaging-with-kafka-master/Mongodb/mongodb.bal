//Jeremiah
//Silas

import ballerinax/mongodb;

mongodb:ConnectionConfig mongoConfig = {
    connection: {
        host: <YOUR_HOST_NAME>,
        port: <PORT>,
        auth: {
            username: <DB_USERNAME>,
            password: <DB_PASSWORD>
        },
        options: {
            sslEnabled: false, 
            serverSelectionTimeout: 5000
        }
    },
    databaseName: DATABASE_NAME>
};
mongodb:Client mongoClient = check new (mongoConfig);

public function main() returns error? {
    
    string collection = "<COLLECTION_NAME>"
    map<json> doc = { "name": "Gmail", "version": "0.99.1", "type" : "Service" };

    check mongoClient->insert(doc, collection);

    mongoClient->close();
}